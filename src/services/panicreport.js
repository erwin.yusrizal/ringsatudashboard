/* jshint esversion: 6 */

import Vue from 'vue';

export const getPanicReports = (payload) => {
    return Vue.http.get('/panic/reports', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addPanicReport = (payload) => {
    return Vue.http.post('/panic/reports', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getPanicReport = (payload) => {
    return Vue.http.get('/panic/reports/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editPanicReport = (payload) => {
    return Vue.http.put('/panic/reports/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deletePanicReport = (payload) => {
    return Vue.http.delete('/panic/reports/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};