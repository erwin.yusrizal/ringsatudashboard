/* jshint esversion: 6 */

import Vue from 'vue';

export const getCategories = (payload) => {
    return Vue.http.get('/categories', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addCategory = (payload) => {
    return Vue.http.post('/categories', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getCategory = (payload) => {
    return Vue.http.get('/categories/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editCategory = (payload) => {
    return Vue.http.put('/categories/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteCategory = (payload) => {
    return Vue.http.delete('/categories/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};