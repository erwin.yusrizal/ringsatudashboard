/* jshint esversion: 6 */

import Vue from 'vue';

export const getMovements = (payload) => {
    return Vue.http.get('/inventory/movements', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addMovement = (payload) => {
    return Vue.http.post('/inventory/movements', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getMovement = (payload) => {
    return Vue.http.get('/inventory/movements/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editMovement = (payload) => {
    return Vue.http.put('/inventory/movements/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteMovement = (payload) => {
    return Vue.http.delete('/inventory/movements/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};