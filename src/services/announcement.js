/* jshint esversion: 6 */

import Vue from 'vue';

export const getAnnouncements = (payload) => {
    return Vue.http.get('/announcements', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addAnnouncement = (payload) => {
    return Vue.http.post('/announcements', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getAnnouncement = (payload) => {
    return Vue.http.get('/announcements/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editAnnouncement = (payload) => {
    return Vue.http.put('/announcements/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteAnnouncement = (payload) => {
    return Vue.http.delete('/announcements/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};