/* jshint esversion: 6 */

import Vue from 'vue';

export const getMedias = (payload) => {
    return Vue.http.get('/media', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addMedia = (payload) => {
    return Vue.http.post('/media', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getMedia = (payload) => {
    return Vue.http.get('/media/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editMedia = (payload) => {
    return Vue.http.put('/media/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteMedia = (payload) => {
    return Vue.http.delete('/media/'+payload.endpoint+'/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteMediaTmp = (payload) => {
    return Vue.http.delete('/media/tmp/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};