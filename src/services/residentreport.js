/* jshint esversion: 6 */

import Vue from 'vue';

export const getResidentReports = (payload) => {
    return Vue.http.get('/residentreports', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addResidentReport = (payload) => {
    return Vue.http.post('/residentreports', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getResidentReport = (payload) => {
    return Vue.http.get('/residentreports/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editResidentReport = (payload) => {
    return Vue.http.put('/residentreports/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteResidentReport = (payload) => {
    return Vue.http.delete('/residentreports/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};