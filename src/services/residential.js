/* jshint esversion: 6 */

import Vue from 'vue';

export const getResidentials = (payload) => {
    return Vue.http.get('/residentials', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addResidential = (payload) => {
    return Vue.http.post('/residentials', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getResidential = (payload) => {
    return Vue.http.get('/residentials/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editResidential = (payload) => {
    return Vue.http.put('/residentials/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteResidential = (payload) => {
    return Vue.http.delete('/residentials/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};