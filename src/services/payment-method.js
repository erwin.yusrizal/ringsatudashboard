/* jshint esversion: 6 */

import Vue from 'vue';

export const getPaymentMethods = (payload) => {
    return Vue.http.get('/payment-methods', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addPaymentMethod = (payload) => {
    return Vue.http.post('/payment-methods', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getPaymentMethod = (payload) => {
    return Vue.http.get('/payment-methods/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editPaymentMethod = (payload) => {
    return Vue.http.put('/payment-methods/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deletePaymentMethod = (payload) => {
    return Vue.http.delete('/payment-methods/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};