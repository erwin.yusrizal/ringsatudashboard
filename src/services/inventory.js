/* jshint esversion: 6 */

import Vue from 'vue';

export const getInventories = (payload) => {
    return Vue.http.get('/inventory', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addInventory = (payload) => {
    return Vue.http.post('/inventory', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getInventory = (payload) => {
    return Vue.http.get('/inventory/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editInventory = (payload) => {
    return Vue.http.put('/inventory/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteInventory = (payload) => {
    return Vue.http.delete('/inventory/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};