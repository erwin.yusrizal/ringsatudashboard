/* jshint esversion: 6 */

import Vue from 'vue';

export const getRegions = (payload) => {
    return Vue.http.get('/regions', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addRegion = (payload) => {
    return Vue.http.post('/regions', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getRegion = (payload) => {
    return Vue.http.get('/regions/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editRegion = (payload) => {
    return Vue.http.put('/regions/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteRegion = (payload) => {
    return Vue.http.delete('/regions/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};