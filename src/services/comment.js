/* jshint esversion: 6 */

import Vue from 'vue';

export const getComments = (payload) => {
    return Vue.http.get('/comments', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addComment = (payload) => {
    return Vue.http.post('/comments', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getComment = (payload) => {
    return Vue.http.get('/comments/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editComment = (payload) => {
    return Vue.http.put('/comments/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteComment = (payload) => {
    return Vue.http.delete('/comments/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};