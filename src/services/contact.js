/* jshint esversion: 6 */

import Vue from 'vue';

export const getContacts = (payload) => {
    return Vue.http.get('/contacts', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addContact = (payload) => {
    return Vue.http.post('/contacts', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getContact = (payload) => {
    return Vue.http.get('/contacts/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editContact = (payload) => {
    return Vue.http.put('/contacts/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteContact = (payload) => {
    return Vue.http.delete('/contacts/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};