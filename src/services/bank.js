/* jshint esversion: 6 */

import Vue from 'vue';

export const getBanks = (payload) => {
    return Vue.http.get('/banks', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addBank = (payload) => {
    return Vue.http.post('/banks', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getBank = (payload) => {
    return Vue.http.get('/banks/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editBank = (payload) => {
    return Vue.http.put('/banks/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteBank = (payload) => {
    return Vue.http.delete('/banks/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};