/* jshint esversion: 6 */

import Vue from 'vue';

export const getCCTVs = (payload) => {
    return Vue.http.get('/cctv', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addCCTV = (payload) => {
    return Vue.http.post('/cctv', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getCCTV = (payload) => {
    return Vue.http.get('/cctv/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editCCTV = (payload) => {
    return Vue.http.put('/cctv/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteCCTV = (payload) => {
    return Vue.http.delete('/cctv/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};