/* jshint esversion: 6 */

import Vue from 'vue';

export const getUnits = (payload) => {
    return Vue.http.get('/units', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addUnit = (payload) => {
    return Vue.http.post('/units', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getUnit = (payload) => {
    return Vue.http.get('/units/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editUnit = (payload) => {
    return Vue.http.put('/units/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteUnit = (payload) => {
    return Vue.http.delete('/units/'+payload.id)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};