/* jshint esversion: 6 */

import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

/* Layout */
import Layout from '@/views/layout/Layout';


/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
	roles: ['admin','editor']    will control the page roles (you can set multiple roles)
	title: 'title'               the name show in sub-menu and breadcrumb (recommend set)
	icon: 'icon class' 	         the icon show in the sidebar
	noCache: true                if true, the page will no be cached(default is false)
	breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
	affix: true                  if true, the tag will affix in the tags-view
  }
**/

export const baseRoutes = [{
	path: '/redirect',
	component: Layout,
	hidden: true,
	children: [{
		path: '/redirect/:path*',
		component: () => import('@/views/redirect/index')
	}]
},{
	path: '/login',
	component: () => import('@/views/login/index'),
	hidden: true
},{
	path: '/auth-redirect',
	component: () => import('@/views/login/authredirect'),
	hidden: true
},{
	path: '/404',
	component: () => import('@/views/error/404'),
	hidden: true
},{
	path: '/401',
	component: () => import('@/views/error/401'),
	hidden: true
},{
	path: '',
	component: Layout,
	redirect: 'dashboard',
	children: [{
		path: 'dashboard',
		component: () => import('@/views/dashboard/index'),
		name: 'Dashboard',
		meta: { 
			title: 'Dashboard', 
			icon:'ion-ios-apps',
			color: '#1989fa',
			noCache: true, 
			affix: true 
		}
	}]
}];

export const asyncRoutes = [{
	path: '/me',
	component: Layout,
	redirect: '/me/profile',
	hidden: true,
	meta: {
		title: 'Profil',
		icon: 'ion-ios-person'
	},
	children: [{
		path: 'profile',
        component: () => import('@/views/me/profile'),
        name: 'myProfile',
        meta: {
			title: 'Profil'
        }
	}, {
		path: 'account',
        component: () => import('@/views/me/account'),
        name: 'myAccount',
        meta: {
			title: 'Akun'
        }
	}, {
		path: 'histories',
        component: () => import('@/views/me/histories'),
        name: 'myHistories',
        meta: {
			title: 'Riwayat'
        }
	}]

}, {
	path: '/security',
	component: Layout,
	redirect: '/security/cctv',
	alwaysShow: true,
	meta: {
		title: 'Keamanan',
		icon: 'ion-ios-videocam',
		color: '#81C5F1',
		roles: ['root']
	},
	children: [{
		path: 'cctv',
        component: () => import('@/views/security/cctv'),
        name: 'cctv',
        meta: {
			title: 'CCTV',
			roles: ['root'],
			module: 'cctv'
		}
	}, {
		path: 'panic-button',
        component: () => import('@/views/security/panics'),
        name: 'panicbutton',
        meta: {
			title: 'Tombol Panik',
			roles: ['root'],
			module: 'panic'
		}

	}, {
		path: 'panic-reports',
        component: () => import('@/views/security/panicreports'),
        name: 'panicreports',
        meta: {
			title: 'Laporan Panik',
			roles: ['root'],
			module: 'panicreport'
		}

	}]

}, {
	path: '/marketplace',
	component: Layout,
	redirect: '/marketplace/products',
	alwaysShow: false,
	meta: {
		title: 'Marketplace',
		icon: 'ion-ios-cart',
		color: '#C8B2F4',
		roles: ['root']
	},
	children: [{
		path: 'products',
        component: () => import('@/views/marketplace/products'),
        name: 'products',
        meta: {
			title: 'Produk',
			roles: ['root'],
			module: 'product'
		}
	}, {
		path: 'orders',
        component: () => import('@/views/marketplace/products'),
        name: 'orders',
        meta: {
			title: 'Pesanan',
			roles: ['root'],
			module: 'order'
		}
	}]

}, {
	path: '/residentials',
	component: Layout,
	redirect: '/residentials/residentials',
	alwaysShow: true,
	meta: {
		title: 'Perumahan',
		icon: 'ion-ios-home',
		color: '#FACB8C',
		roles: ['root']
	},
	children: [{
		path: 'infos',
        component: () => import('@/views/residentials/info'),
        name: 'infos',
        meta: {
			title: 'Informasi',
			roles: ['root'],
			module: 'info'
		}
	}, {
		path: 'comments',
        component: () => import('@/views/residentials/comments'),
        name: 'comments',
        meta: {
			title: 'Komentar',
			roles: ['root'],
			module: 'comment'
		}
	}, {
		path: 'residentials',
        component: () => import('@/views/residentials/residentials'),
        name: 'residentials',
        meta: {
			title: 'Perumahan',
			roles: ['root'],
			module: 'residential'
		}
	}, {
		path: 'inventory',
        component: () => import('@/views/residentials/inventory'),
        name: 'inventory',
        meta: {
			title: 'Inventaris',
			roles: ['root'],
			module: 'inventory'
		}
	}, {
		path: 'inventory-movements',
        component: () => import('@/views/residentials/movements'),
        name: 'inventorymovements',
        meta: {
			title: 'Pergerakan Inventaris',
			roles: ['root'],
			module: 'inventorymovement'
		}
	}, {
		path: 'announcement',
        component: () => import('@/views/residentials/announcements'),
        name: 'announcement',
        meta: {
			title: 'Pemberitahuan',
			roles: ['root'],
			module: 'announcement'
		}
	}, {
		path: 'resident-reports',
        component: () => import('@/views/residentials/residentreports'),
        name: 'residentreports',
        meta: {
			title: 'Laporan Warga',
			roles: ['root'],
			module: 'residentreport'
		}
	}, {
		path: 'financial-reports',
        component: () => import('@/views/residentials/financialreports'),
        name: 'financialreports',
        meta: {
			title: 'Laporan Keuangan',
			roles: ['root'],
			module: 'financialreport'
		}
	}]

}, {
	path: '/people',
	component: Layout,
	redirect: '/people/roles',
	alwaysShow: true,
	meta: {
		title: 'Pengguna',
		icon: 'ion-ios-person',
		color: '#ED858B',
		roles: ['root']
	},
	children: [{
		path: 'roles',
        component: () => import('@/views/people/roles'),
        name: 'roles',
        meta: {
			title: 'Hak Akses',
			roles: ['root'],
			module: 'role'
		}	
	}, {
		path: 'users',
        component: () => import('@/views/people/users'),
        name: 'users',
        meta: {
			title: 'Pengguna',
			roles: ['root'],
			module: 'user'
		}	
	}, {
		path: 'histories',
        component: () => import('@/views/people/roles'),
        name: 'histories',
        meta: {
			title: 'Riwayat',
			roles: ['root'],
			module: 'history'
		}
	}]

}, {
	path: '/settings',
	component: Layout,
	redirect: '/settings/bank',
	alwaysShow: true,
	meta: {
		title: 'Pengaturan',
		icon: 'ion-ios-construct',
		color: '#6FC5AA',
		roles: ['root']
	},
	children: [{
		path: 'banks',
        component: () => import('@/views/settings/banks'),
        name: 'banks',
        meta: {
			title: 'Bank',
			roles: ['root'],
			module: 'bank'
		}
	}, {
		path: 'categories',
		component: () => import('@/views/settings/categories'),
		name: 'categories',
		meta: {
			title: 'Kategori',
			roles: ['root'],
			module: 'category'
		}	
	}, {
		path: 'payment-methods',
        component: () => import('@/views/settings/payment-methods'),
        name: 'paymentmethods',
        meta: {
			title: 'Metode Pembayaran',
			roles: ['root'],
			module: 'paymentmethod'
		}
	}, {
		path: 'contact',
        component: () => import('@/views/settings/contacts'),
        name: 'contact',
        meta: {
			title: 'No. Telp Penting',
			roles: ['root'],
			module: 'contact'
		}
	}, {
		path: 'units',
        component: () => import('@/views/settings/units'),
        name: 'units',
        meta: {
			title: 'Satuan Barang',
			roles: ['root'],
			module: 'unit'
        }
	}, {
		path: 'regions',
        component: () => import('@/views/settings/regions'),
        name: 'regions',
        meta: {
			title: 'Wilayah',
			roles: ['root'],
			module: 'region'
        }
	}]
	
}, { path: '*', redirect: '/404', hidden: true }];


const createRouter = () => new Router({
	routes: baseRoutes,
	mode: 'history',
	scrollBehavior: () => ({ y: 0 }),
});

const router = createRouter();

export function resetRouter() {
	const newRouter = createRouter()
	router.matcher = newRouter.matcher;
}
  
export default router;