/* jshint esversion:6 */

const title = 'RingSatu - Building Living Neighborhoods';

export default function getPageTitle(key) {
    const hasKey = key;
    if (hasKey) {
        const pageName = key;
        return `${pageName} - ${title}`;
    }
    return `${title}`;
}
