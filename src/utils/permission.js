/* jshint esversion: 6 */

import store from '@/store';

export default function checkPermission(value) {
    if (value && value instanceof Array && value.length > 0) {
        const roles = store.getters && store.getters['role/roles'];    
        const [key, val] = value;
      
        const hasPermission = roles.some(role => {
            return role.permissions[key][val] > 0;
        });
    
        if (!hasPermission) {
            return false;
        }
        return true;
    } else {
        return false;
    }
}