/*jshint esversion: 6 */

import Vue from 'vue';
import axios from 'axios';
import App from './App';
import router from './router';
import store from './store';
import CKEditor from '@ckeditor/ckeditor5-vue';

import 'normalize.css/normalize.css';
import 'vue2-animate/dist/vue2-animate.min.css';

import VueCountdown from '@chenfengyuan/vue-countdown';

import * as VueGoogleMaps from 'vue2-google-maps';

import Element from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en';
import 'element-ui/lib/theme-chalk/index.css';
import '@/styles/index.scss';

import './logs';
import './permission';


Number.prototype.format = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};


Vue.config.productionTip = false;
Vue.use(Element, { locale });
Vue.use( CKEditor );

Vue.component(VueCountdown.name, VueCountdown);

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyCkn9evlNr7R84B7N5a1pZ1cumYBmiZOt4',
        libraries: 'places,drawing,geometry'
    },
    installComponents: true
});

Vue.http = axios.create({
	baseURL: 'http://api.ringsatu.id/v1',
	withCredentials: false,
	headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json'
	}
});

Vue.http.interceptors.request.use(
    config => {
        if(store.getters['auth/isLoggedIn']){
            if(config.url.indexOf('auth/revoke/refresh') === -1 && config.url.indexOf('auth/refresh') === -1){
                config.headers.Authorization = "Bearer " + store.state.auth.token.access_token;
            }else if(config.url.indexOf('auth/revoke/refresh') > -1){
                /** Erwin: this vuex-persistedstate gimme a shit by doing this
                /* the author doesnt gimme any option to clear the state programmatically
                */
                localStorage.removeItem('vuex');                
            }
        }
        return config;
    }, 
    error => {

        if(error.response.status == 401){
            store.dispatch('auth/doLogout');
        }

        Promise.reject(error.response.data);
    }
);

Vue.http.interceptors.response.use(
    response => {
        if(response.config.url.indexOf('auth/revoke/refresh') > -1){
            location.reload();
        }
        return response.data;        
    },
    error => {
		if (error.response.status === 401) {
            store.dispatch('auth/doLogout');
		}
        return Promise.reject(error.response.data);
    }
);

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app');
