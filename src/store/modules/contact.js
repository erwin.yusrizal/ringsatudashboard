/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getContacts, addContact, editContact, deleteContact } from '@/services/contact';

const contact = {
    namespaced: true,
    state: {
        contacts: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        contacts: state => state.contacts,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_CONTACTS: (state, payload) => {
            state.contacts = payload.contacts;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_CONTACT: (state, contact) => {
            state.contacts.push(contact);
        },
        EDIT_CONTACT: (state, contact) => {
            for(let i of state.contacts){
                if(i.id == contact.id){
                    i = Object.assign(i, contact);
                    break;
                }
            }
        },
        DELETE_CONTACT: (state, contact) => {
            for(let i of state.contacts){
                if(i.id == contact.id){
                    const index = state.contacts.indexOf(i);
                    state.contacts.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getContacts({ commit }, payload){
            try{
                let response = await getContacts(payload);
                commit('ADD_CONTACTS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addContact({ commit }, payload){
            try{
                let response = await addContact(payload);
                let data = response.data;
                commit('ADD_CONTACT', data.contact);
                return response;
            }catch(e){
                return e;
            }
        },
        async editContact({ commit }, payload){
            try{
                let response = await editContact(payload);
                let data = response.data;
                commit('EDIT_CONTACT', data.contact);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteContact({ commit }, payload){
            try{
                let response = await deleteContact(payload);
                let data = response.data;
                commit('DELETE_CONTACT', data.contact);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default contact;