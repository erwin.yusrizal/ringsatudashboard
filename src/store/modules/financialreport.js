/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getFinancialReports, addFinancialReport, editFinancialReport, deleteFinancialReport } from '@/services/financialreport';

const financialreport = {
    namespaced: true,
    state: {
        financialreports: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        financialreports: state => state.financialreports,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_FINANCIAL_REPORTS: (state, payload) => {
            state.financialreports = payload.financialreports;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_FINANCIAL_REPORT: (state, financialreport) => {
            state.financialreports.push(financialreport);
        },
        EDIT_FINANCIAL_REPORT: (state, financialreport) => {
            for(let i of state.financialreports){
                if(i.id == financialreport.id){
                    i = Object.assign(i, financialreport);
                    break;
                }
            }
        },
        DELETE_FINANCIAL_REPORT: (state, financialreport) => {
            for(let i of state.financialreports){
                if(i.id == financialreport.id){
                    const index = state.financialreports.indexOf(i);
                    state.financialreports.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getFinancialReports({ commit }, payload){
            try{
                let response = await getFinancialReports(payload);
                commit('ADD_FINANCIAL_REPORTS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addFinancialReport({ commit }, payload){
            try{
                let response = await addFinancialReport(payload);
                let data = response.data;
                commit('ADD_FINANCIAL_REPORT', data.financialreport);
                return response;
            }catch(e){
                return e;
            }
        },
        async editFinancialReport({ commit }, payload){
            try{
                let response = await editFinancialReport(payload);
                let data = response.data;
                commit('EDIT_FINANCIAL_REPORT', data.financialreport);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteFinancialReport({ commit }, payload){
            try{
                let response = await deleteFinancialReport(payload);
                let data = response.data;
                commit('DELETE_FINANCIAL_REPORT', data.financialreport);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default financialreport;