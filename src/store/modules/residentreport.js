/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getResidentReports, addResidentReport, editResidentReport, deleteResidentReport } from '@/services/residentreport';

const residentreport = {
    namespaced: true,
    state: {
        residentreports: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        residentreports: state => state.residentreports,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_RESIDENT_REPORTS: (state, payload) => {
            state.residentreports = payload.residentreports;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_RESIDENT_REPORT: (state, residentreport) => {
            state.residentreports.push(residentreport);
        },
        EDIT_RESIDENT_REPORT: (state, residentreport) => {
            for(let i of state.residentreports){
                if(i.id == residentreport.id){
                    i = Object.assign(i, residentreport);
                    break;
                }
            }
        },
        DELETE_RESIDENT_REPORT: (state, residentreport) => {
            for(let i of state.residentreports){
                if(i.id == residentreport.id){
                    const index = state.residentreports.indexOf(i);
                    state.residentreports.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getResidentReports({ commit }, payload){
            try{
                let response = await getResidentReports(payload);
                commit('ADD_RESIDENT_REPORTS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addResidentReport({ commit }, payload){
            try{
                let response = await addResidentReport(payload);
                let data = response.data;
                commit('ADD_RESIDENT_REPORT', data.residentreport);
                return response;
            }catch(e){
                return e;
            }
        },
        async editResidentReport({ commit }, payload){
            try{
                let response = await editResidentReport(payload);
                let data = response.data;
                commit('EDIT_RESIDENT_REPORT', data.residentreport);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteResidentReport({ commit }, payload){
            try{
                let response = await deleteResidentReport(payload);
                let data = response.data;
                commit('DELETE_RESIDENT_REPORT', data.residentreport);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default residentreport;