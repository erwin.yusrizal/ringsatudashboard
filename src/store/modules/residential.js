/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getResidentials, addResidential, editResidential, deleteResidential } from '@/services/residential';

const residential = {
    namespaced: true,
    state: {
        residentials: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        residentials: state => state.residentials,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_RESIDENTIALS: (state, payload) => {
            state.residentials = payload.residentials;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_RESIDENTIAL: (state, residential) => {
            state.residentials.push(residential);
        },
        EDIT_RESIDENTIAL: (state, residential) => {
            for(let i of state.residentials){
                if(i.id == residential.id){
                    i = Object.assign(i, residential);
                    break;
                }
            }
        },
        DELETE_RESIDENTIAL: (state, residential) => {
            for(let i of state.residentials){
                if(i.id == residential.id){
                    const index = state.residentials.indexOf(i);
                    state.residentials.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getResidentials({ commit }, payload){
            try{
                let response = await getResidentials(payload);
                commit('ADD_RESIDENTIALS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addResidential({ commit }, payload){
            try{
                let response = await addResidential(payload);
                let data = response.data;
                commit('ADD_RESIDENTIAL', data.residential);
                return response;
            }catch(e){
                return e;
            }
        },
        async editResidential({ commit }, payload){
            try{
                let response = await editResidential(payload);
                let data = response.data;
                commit('EDIT_RESIDENTIAL', data.residential);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteResidential({ commit }, payload){
            try{
                let response = await deleteResidential(payload);
                let data = response.data;
                commit('DELETE_RESIDENTIAL', data.residential);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default residential;