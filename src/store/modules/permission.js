/* jshint esversion: 6 */

import { baseRoutes, asyncRoutes } from '@/router';

function hasPermission(role, route){
    if(route.meta && route.meta.module){
        return !!role.permissions[route.meta.module].menu;
    }else{
        return true;
    }
}

export function filterAsyncRoutes(routes, role){
    const res = [];
    routes.forEach(route => {
        const tmp = {...route};
        if(hasPermission(role, tmp)){
            if(tmp.children){
                tmp.children = filterAsyncRoutes(tmp.children, role);
            }
            res.push(tmp);
        }
    });

    return res;
}

const permission = {
    namespaced: true,
    state: {
        routes: [],
        addRoutes: []
    },
    getters: {
        permissionRoutes: state => state.routes
    },
    mutations: {
        SET_ROUTES: (state, routes) => {
            state.addRoutes = routes;
            state.routes = baseRoutes.concat(routes);
        }
    },
    actions: {
        async generateRoutes({ commit }, role){
            let accessedRoutes;
            if(role.slug == 'root'){
                accessedRoutes = asyncRoutes || [];
            }else{
                accessedRoutes = await filterAsyncRoutes(asyncRoutes, role);
            }
            commit('SET_ROUTES', accessedRoutes);
            return accessedRoutes;
        }
    }
};

export default permission;