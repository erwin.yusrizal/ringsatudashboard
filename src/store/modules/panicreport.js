/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getPanicReports, addPanicReport, editPanicReport, deletePanicReport } from '@/services/panicreport';

const panicreport = {
    namespaced: true,
    state: {
        panicreports: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        panicreports: state => state.panicreports,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_PANIC_REPORTS: (state, payload) => {
            state.panicreports = payload.panicreports;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_PANIC_REPORT: (state, panicreport) => {
            state.panicreports.push(panicreport);
        },
        EDIT_PANIC_REPORT: (state, panicreport) => {
            for(let i of state.panicreports){
                if(i.id == panicreport.id){
                    i = Object.assign(i, panicreport);
                    break;
                }
            }
        },
        DELETE_PANIC_REPORT: (state, panicreport) => {
            for(let i of state.panicreports){
                if(i.id == panicreport.id){
                    const index = state.panicreports.indexOf(i);
                    state.panicreports.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getPanicReports({ commit }, payload){
            try{
                let response = await getPanicReports(payload);
                commit('ADD_PANIC_REPORTS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addPanicReport({ commit }, payload){
            try{
                let response = await addPanicReport(payload);
                let data = response.data;
                commit('ADD_PANIC_REPORT', data.panicreport);
                return response;
            }catch(e){
                return e;
            }
        },
        async editPanicReport({ commit }, payload){
            try{
                let response = await editPanicReport(payload);
                let data = response.data;
                commit('EDIT_PANIC_REPORT', data.panicreport);
                return response;
            }catch(e){
                return e;
            }
        },
        async deletePanicReport({ commit }, payload){
            try{
                let response = await deletePanicReport(payload);
                let data = response.data;
                commit('DELETE_PANIC_REPORT', data.panicreport);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default panicreport;