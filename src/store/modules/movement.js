/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getMovements, addMovement, editMovement, deleteMovement } from '@/services/movement';

const movement = {
    namespaced: true,
    state: {
        movements: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        movements: state => state.movements,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_MOVEMENTS: (state, payload) => {
            state.movements = payload.movements;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_MOVEMENT: (state, movement) => {
            state.movements.push(movement);
        },
        EDIT_MOVEMENT: (state, movement) => {
            for(let i of state.movements){
                if(i.id == movement.id){
                    i = Object.assign(i, movement);
                    break;
                }
            }
        },
        DELETE_MOVEMENT: (state, movement) => {
            for(let i of state.movements){
                if(i.id == movement.id){
                    const index = state.movements.indexOf(i);
                    state.movements.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getMovements({ commit }, payload){
            try{
                let response = await getMovements(payload);
                commit('ADD_MOVEMENTS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addMovement({ commit }, payload){
            try{
                let response = await addMovement(payload);
                let data = response.data;
                commit('ADD_MOVEMENT', data.movement);
                return response;
            }catch(e){
                return e;
            }
        },
        async editMovement({ commit }, payload){
            try{
                let response = await editMovement(payload);
                let data = response.data;
                commit('EDIT_MOVEMENT', data.movement);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteMovement({ commit }, payload){
            try{
                let response = await deleteMovement(payload);
                let data = response.data;
                commit('DELETE_MOVEMENT', data.movement);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default movement;