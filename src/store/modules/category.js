/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getCategories, addCategory, editCategory, deleteCategory } from '@/services/category';

const category = {
    namespaced: true,
    state: {
        categories: [],
        parentCategories: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        categories: state => state.categories,
        parentCategories: state => state.parentCategories,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_CATEGORIES: (state, payload) => {
            state.categories = payload.categories;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_PARENT_CATEGORIES: (state, payload) => {
            state.parentCategories = payload.categories;
        },
        ADD_CATEGORY: (state, category) => {
            state.categories.push(category);
        },
        EDIT_CATEGORY: (state, category) => {
            for(let i of state.categories){
                if(i.id == category.id){
                    i = Object.assign(i, category);
                    break;
                }
            }
        },
        DELETE_CATEGORY: (state, category) => {
            for(let i of state.categories){
                if(i.id == category.id){
                    const index = state.categories.indexOf(i);
                    state.categories.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getCategories({ commit }, payload){
            try{
                let response = await getCategories(payload);
                commit('ADD_CATEGORIES', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async getParentCategories({ commit }, payload){
            try{
                let response = await getCategories(payload);
                commit('ADD_PARENT_CATEGORIES', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addCategory({ commit }, payload){
            try{
                let response = await addCategory(payload);
                let data = response.data;
                commit('ADD_CATEGORY', data.category);
                return response;
            }catch(e){
                return e;
            }
        },
        async editCategory({ commit }, payload){
            try{
                let response = await editCategory(payload);
                let data = response.data;
                commit('EDIT_CATEGORY', data.category);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteCategory({ commit }, payload){
            try{
                let response = await deleteCategory(payload);
                let data = response.data;
                commit('DELETE_CATEGORY', data.category);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default category;