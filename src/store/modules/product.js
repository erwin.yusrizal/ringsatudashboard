/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getProducts, addProduct, editProduct, deleteProduct } from '@/services/product';

const product = {
    namespaced: true,
    state: {
        products: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        products: state => state.products,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_PRODUCTS: (state, payload) => {
            state.products = payload.products;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_PRODUCT: (state, product) => {
            state.products.push(product);
        },
        EDIT_PRODUCT: (state, product) => {
            for(let i of state.products){
                if(i.id == product.id){
                    i = Object.assign(i, product);
                    break;
                }
            }
        },
        DELETE_PRODUCT: (state, product) => {
            for(let i of state.products){
                if(i.id == product.id){
                    const index = state.products.indexOf(i);
                    state.products.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getProducts({ commit }, payload){
            try{
                let response = await getProducts(payload);
                commit('ADD_PRODUCTS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addProduct({ commit }, payload){
            try{
                let response = await addProduct(payload);
                let data = response.data;
                commit('ADD_PRODUCT', data.product);
                return response;
            }catch(e){
                return e;
            }
        },
        async editProduct({ commit }, payload){
            try{
                let response = await editProduct(payload);
                let data = response.data;
                commit('EDIT_PRODUCT', data.product);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteProduct({ commit }, payload){
            try{
                let response = await deleteProduct(payload);
                let data = response.data;
                commit('DELETE_PRODUCT', data.product);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default product;