/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getInfos, addInfo, editInfo, deleteInfo } from '@/services/info';

const info = {
    namespaced: true,
    state: {
        infos: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        infos: state => state.infos,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_INFOS: (state, payload) => {
            state.infos = payload.infos;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_INFO: (state, info) => {
            state.infos.push(info);
        },
        EDIT_INFO: (state, info) => {
            for(let i of state.infos){
                if(i.id == info.id){
                    i = Object.assign(i, info);
                    break;
                }
            }
        },
        DELETE_INFO: (state, info) => {
            for(let i of state.infos){
                if(i.id == info.id){
                    const index = state.infos.indexOf(i);
                    state.infos.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getInfos({ commit }, payload){
            try{
                let response = await getInfos(payload);
                commit('ADD_INFOS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addInfo({ commit }, payload){
            try{
                let response = await addInfo(payload);
                let data = response.data;
                commit('ADD_INFO', data.info);
                return response;
            }catch(e){
                return e;
            }
        },
        async editInfo({ commit }, payload){
            try{
                let response = await editInfo(payload);
                let data = response.data;
                commit('EDIT_INFO', data.info);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteInfo({ commit }, payload){
            try{
                let response = await deleteInfo(payload);
                let data = response.data;
                commit('DELETE_INFO', data.info);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default info;