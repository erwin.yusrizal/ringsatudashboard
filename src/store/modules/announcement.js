/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getAnnouncements, addAnnouncement, editAnnouncement, deleteAnnouncement } from '@/services/announcement';

const announcement = {
    namespaced: true,
    state: {
        announcements: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        announcements: state => state.announcements,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_ANNOUNCEMENTS: (state, payload) => {
            state.announcements = payload.announcements;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_ANNOUNCEMENT: (state, announcement) => {
            state.announcements.push(announcement);
        },
        EDIT_ANNOUNCEMENT: (state, announcement) => {
            for(let i of state.announcements){
                if(i.id == announcement.id){
                    i = Object.assign(i, announcement);
                    break;
                }
            }
        },
        DELETE_ANNOUNCEMENT: (state, announcement) => {
            for(let i of state.announcements){
                if(i.id == announcement.id){
                    const index = state.announcements.indexOf(i);
                    state.announcements.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getAnnouncements({ commit }, payload){
            try{
                let response = await getAnnouncements(payload);
                commit('ADD_ANNOUNCEMENTS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addAnnouncement({ commit }, payload){
            try{
                let response = await addAnnouncement(payload);
                let data = response.data;
                commit('ADD_ANNOUNCEMENT', data.announcement);
                return response;
            }catch(e){
                return e;
            }
        },
        async editAnnouncement({ commit }, payload){
            try{
                let response = await editAnnouncement(payload);
                let data = response.data;
                commit('EDIT_ANNOUNCEMENT', data.announcement);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteAnnouncement({ commit }, payload){
            try{
                let response = await deleteAnnouncement(payload);
                let data = response.data;
                commit('DELETE_ANNOUNCEMENT', data.announcement);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default announcement;