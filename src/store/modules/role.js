/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getPermissions, getRoles, addRole, editRole, deleteRole, editRolePermission } from '@/services/role';
import { async } from 'q';

const role = {
    namespaced: true,
    state: {
        permissions: [],
        permissionRoles: [],
        defaultPermissions: {},
        roles: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        }        
    },
    getters: {
        permissions: state => state.permissions,
        defaultPermissions: state => state.defaultPermissions,
        permissionRoles: state => state.permissionRoles,
        roles: state => state.roles,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_PERMISSIONS: (state, permissions) => {
            state.permissions = permissions;

            let defaultPermissions = {};

            for(const permission of permissions){
                defaultPermissions[permission.slug] = {
                    'menu': 0,
                    'viewall': 0,
                    'create': 0,
                    'viewown': 0,
                    'viewother': 0,
                    'editown': 0,
                    'editother': 0,
                    'deleteown': 0,
                    'deleteother': 0
                };
            }

            state.defaultPermissions = defaultPermissions;
        },
        ADD_ROLES: (state, payload) => {
            state.roles = payload.roles;
            Object.assign(state.pagination, payload.pagination);
        },
        ADD_PERMISSION_ROLES: (state, roles) => {
            state.permissionRoles = roles.map(role => role.slug);
        },
        CLEAR_PERMISSION_ROLES: (state) => {
            state.permissionRoles = [];
        },
        ADD_ROLE_SUCCESS: (state, role) => {
            state.roles.push(role);            
        },
        EDIT_ROLE_SUCCESS: (state, role) => {
            for(let i of state.roles){
                if(i.id == role.id){
                    i = Object.assign(i, role);
                    break;
                }
            }
        },
        DELETE_ROLE_SUCCESS: (state, role) => {
            for(let i of state.roles){
                if(i.id == role.id){
                    const index = state.roles.indexOf(i);
                    state.roles.splice(index, 1);
                    break;
                }
            }
        },
        UPDATE_DEFAULT_PERMISSION: (state, payload) => {
            state.defaultPermissions[payload.module][payload.permission] = payload.value;
        },
        UPDATE_ROLE_PERMISSION_SUCCESS: (state, permissions) => {
            let defaultPermissions = {};

            for(const i in permissions){
                defaultPermissions[i] = {
                    'viewall': 0,
                    'create': 0,
                    'viewown': 0,
                    'viewother': 0,
                    'editown': 0,
                    'editother': 0,
                    'deleteown': 0,
                    'deleteother': 0
                };
            }

            state.defaultPermissions = defaultPermissions;
        }
    },
    actions: {
        async getAllPermissions({ commit }, payload){
            try{
                let response = await getPermissions(payload);
                let data = response.data;
                commit('ADD_PERMISSIONS', data.permissions);
                return data;
            }catch(e){
                console.log(e);
            }         
        },
        async getAllRoles({ commit }, payload){
            try{
                let response = await getRoles(payload);
                commit('ADD_ROLES', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }         
        },
        async getAllPermissionRoles({ commit }){
            try{
                let response = await getRoles();
                commit('ADD_PERMISSION_ROLES', response.data.roles);
                commit('ADD_ROLES', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }         
        },
        async clearPermissionRoles({ commit }){
            commit('CLEAR_PERMISSION_ROLES');
        },
        async addRole({ commit }, payload){
            try{
                let response = await addRole(payload);
                let data = response.data;
                commit('ADD_ROLE_SUCCESS', data.role);
                return response;
            }catch(e){
                return e;
            }
        },
        async editRole({ commit }, payload){
            try{
                let response = await editRole(payload);
                let data = response.data;
                commit('EDIT_ROLE_SUCCESS', data.role);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteRole({ commit }, payload){
            try{
                let response = await deleteRole(payload);
                let data = response.data;
                commit('DELETE_ROLE_SUCCESS', data.role);
                return response;
            }catch(e){
                return e;
            }
        },
        async updateRolePermission({ commit }, payload){
            try{
                let response = await editRolePermission(payload);
                let data = response.data;
                commit('UPDATE_ROLE_PERMISSION_SUCCESS', data.permissions);
                return response;
            }catch(e){
                return e;
            }
        },
        updateDefaultPermission({ commit }, payload){
            commit('UPDATE_DEFAULT_PERMISSION', payload);
        }
    }
};

export default role;