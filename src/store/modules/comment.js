/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getComments, addComment, editComment, deleteComment } from '@/services/comment';

const comment = {
    namespaced: true,
    state: {
        comments: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        comments: state => state.comments,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_INFOS: (state, payload) => {
            state.comments = payload.comments;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_INFO: (state, comment) => {
            state.comments.push(comment);
        },
        EDIT_INFO: (state, comment) => {
            for(let i of state.comments){
                if(i.id == comment.id){
                    i = Object.assign(i, comment);
                    break;
                }
            }
        },
        DELETE_INFO: (state, comment) => {
            for(let i of state.comments){
                if(i.id == comment.id){
                    const index = state.comments.indexOf(i);
                    state.comments.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getComments({ commit }, payload){
            try{
                let response = await getComments(payload);
                commit('ADD_INFOS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addComment({ commit }, payload){
            try{
                let response = await addComment(payload);
                let data = response.data;
                commit('ADD_INFO', data.comment);
                return response;
            }catch(e){
                return e;
            }
        },
        async editComment({ commit }, payload){
            try{
                let response = await editComment(payload);
                let data = response.data;
                commit('EDIT_INFO', data.comment);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteComment({ commit }, payload){
            try{
                let response = await deleteComment(payload);
                let data = response.data;
                commit('DELETE_INFO', data.comment);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default comment;