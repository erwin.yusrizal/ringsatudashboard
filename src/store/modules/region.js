/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getRegions, addRegion, editRegion, deleteRegion } from '@/services/region';

const region = {
    namespaced: true,
    state: {
        regions: [],
        provinces: [],
        cities: [],
        districts: [],
        subdistricts: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        }
    },
    getters: {
        regions: state => state.regions,
        pagination: state => state.pagination,
        provinces: state => state.provinces,
        cities: state => state.cities,
        districts: state => state.districts,
        subdistricts: state => state.subdistricts
    },
    mutations: {
        ADD_REGIONS: (state, payload) => {
            state.regions = payload.regions;
            Object.assign(state.pagination, payload.pagination);
        },
        ADD_REGION: (state, region) => {
            state.regions.push(region);
        },
        EDIT_REGION: (state, region) => {
            for(let i of state.regions){
                if(i.id == region.id){
                    i = Object.assign(i, region);
                    break;
                }
            }
        },
        DELETE_REGION: (state, region) => {
            for(let i of state.regions){
                if(i.id == region.id){
                    const index = state.regions.indexOf(i);
                    state.regions.splice(index, 1);
                    break;
                }
            }
        },
        ADD_PROVINCES: (state, provinces) => {
            state.provinces = provinces;
        },
        ADD_CITIES: (state, cities) => {
            state.cities = cities;
        },
        ADD_DISTRICTS: (state, districts) => {
            state.districts = districts;
        },
        ADD_SUBDISTRICTS: (state, subdistricts) => {
            state.subdistricts = subdistricts;
        }
    },
    actions: {
        async getRegions({ commit }, payload){
            try{
                let response = await getRegions(payload);
                commit('ADD_REGIONS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addRegion({ commit }, payload){
            try{
                let response = await addRegion(payload);
                let data = response.data;
                commit('ADD_REGION', data.region);
                return response;
            }catch(e){
                return e;
            }
        },
        async editRegion({ commit }, payload){
            try{
                let response = await editRegion(payload);
                let data = response.data;
                commit('EDIT_REGION', data.region);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteRegion({ commit }, payload){
            try{
                let response = await deleteRegion(payload);
                let data = response.data;
                commit('DELETE_REGION', data.region);
                return response;
            }catch(e){
                return e;
            }
        },
        async getProvinces({ commit }, payload){
            try{
                let response = await getRegions(payload);
                commit('ADD_PROVINCES', response.data.regions);
                return response.data;
            }catch(e){
                return e;
            }
        },
        async getCities({ commit }, payload){
            try{
                let response = await getRegions(payload);
                commit('ADD_CITIES', response.data.regions);
                return response.data;
            }catch(e){
                return e;
            }
        },
        async getDistricts({ commit }, payload){
            try{
                let response = await getRegions(payload);
                commit('ADD_DISTRICTS', response.data.regions);
                return response.data;
            }catch(e){
                return e;
            }
        },
        async getSubdistricts({ commit }, payload){
            try{
                let response = await getRegions(payload);
                commit('ADD_SUBDISTRICTS', response.data.regions);
                return response.data;
            }catch(e){
                return e;
            }
        }
    }
};

export default region;