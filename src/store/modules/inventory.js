/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getInventories, addInventory, editInventory, deleteInventory } from '@/services/inventory';

const inventory = {
    namespaced: true,
    state: {
        inventories: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        inventories: state => state.inventories,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_INVENTORIES: (state, payload) => {
            state.inventories = payload.inventories;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_INVENTORY: (state, inventory) => {
            state.inventories.push(inventory);
        },
        EDIT_INVENTORY: (state, inventory) => {
            for(let i of state.inventories){
                if(i.id == inventory.id){
                    i = Object.assign(i, inventory);
                    break;
                }
            }
        },
        DELETE_INVENTORY: (state, inventory) => {
            for(let i of state.inventories){
                if(i.id == inventory.id){
                    const index = state.inventories.indexOf(i);
                    state.inventories.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getInventories({ commit }, payload){
            try{
                let response = await getInventories(payload);
                commit('ADD_INVENTORIES', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addInventory({ commit }, payload){
            try{
                let response = await addInventory(payload);
                let data = response.data;
                commit('ADD_INVENTORY', data.inventory);
                return response;
            }catch(e){
                return e;
            }
        },
        async editInventory({ commit }, payload){
            try{
                let response = await editInventory(payload);
                let data = response.data;
                commit('EDIT_INVENTORY', data.inventory);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteInventory({ commit }, payload){
            try{
                let response = await deleteInventory(payload);
                let data = response.data;
                commit('DELETE_INVENTORY', data.inventory);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default inventory;