/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getMedias, addMedia, editMedia, deleteMedia, deleteMediaTmp } from '@/services/media';

const media = {
    namespaced: true,
    state: {
        medias: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        medias: state => state.medias,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_MEDIAS: (state, payload) => {
            state.medias = payload.medias;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_MEDIA: (state, media) => {
            state.medias.push(media);
        },
        EDIT_MEDIA: (state, media) => {
            for(let i of state.medias){
                if(i.id == media.id){
                    i = Object.assign(i, media);
                    break;
                }
            }
        },
        DELETE_MEDIA: (state, media) => {
            for(let i of state.medias){
                if(i.id == media.id){
                    const index = state.medias.indexOf(i);
                    state.medias.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getMedias({ commit }, payload){
            try{
                let response = await getMedias(payload);
                commit('ADD_MEDIAS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addMedia({ commit }, payload){
            try{
                let response = await addMedia(payload);
                let data = response.data;
                commit('ADD_MEDIA', data.media);
                return response;
            }catch(e){
                return e;
            }
        },
        async editMedia({ commit }, payload){
            try{
                let response = await editMedia(payload);
                let data = response.data;
                commit('EDIT_MEDIA', data.media);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteMedia({ commit }, payload){
            try{
                let response = await deleteMedia(payload);
                let data = response.data;
                commit('DELETE_MEDIA', data.media);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteMediaTmp({commit}, payload){
            try{
                let response = await deleteMediaTmp(payload);
                return response;
            }catch(e){
                return e;
            }
        }
    }
};

export default media;