/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getPaymentMethods, addPaymentMethod, editPaymentMethod, deletePaymentMethod } from '@/services/payment-method';

const paymentMethod = {
    namespaced: true,
    state: {
        paymentMethods: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        paymentMethods: state => state.paymentMethods,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_PAYMENT_METHODS: (state, payload) => {
            state.paymentMethods = payload.payment_methods;
            Object.assign(state.pagination, payload.pagination);
        },
        ADD_PAYMENT_METHOD: (state, paymentMethod) => {
            state.paymentMethods.push(paymentMethod);
        },
        EDIT_PAYMENT_METHOD: (state, paymentMethod) => {
            for(let i of state.paymentMethods){
                if(i.id == paymentMethod.id){
                    i = Object.assign(i, paymentMethod);
                    break;
                }
            }
        },
        DELETE_PAYMENT_METHOD: (state, paymentMethod) => {
            for(let i of state.paymentMethods){
                if(i.id == paymentMethod.id){
                    const index = state.paymentMethods.indexOf(i);
                    state.paymentMethods.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getPaymentMethods({ commit }, payload){
            try{
                let response = await getPaymentMethods(payload);
                commit('ADD_PAYMENT_METHODS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addPaymentMethod({ commit }, payload){
            try{
                let response = await addPaymentMethod(payload);
                let data = response.data;
                commit('ADD_PAYMENT_METHOD', data.payment_method);
                return response;
            }catch(e){
                return e;
            }
        },
        async editPaymentMethod({ commit }, payload){
            try{
                let response = await editPaymentMethod(payload);
                let data = response.data;
                commit('EDIT_PAYMENT_METHOD', data.payment_method);
                return response;
            }catch(e){
                return e;
            }
        },
        async deletePaymentMethod({ commit }, payload){
            try{
                let response = await deletePaymentMethod(payload);
                let data = response.data;
                commit('DELETE_PAYMENT_METHOD', data.payment_method);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default paymentMethod;