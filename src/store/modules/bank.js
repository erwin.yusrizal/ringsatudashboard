/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getBanks, addBank, editBank, deleteBank } from '@/services/bank';

const bank = {
    namespaced: true,
    state: {
        banks: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        banks: state => state.banks,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_BANKS: (state, payload) => {
            state.banks = payload.banks;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_BANK: (state, bank) => {
            state.banks.push(bank);
        },
        EDIT_BANK: (state, bank) => {
            for(let i of state.banks){
                if(i.id == bank.id){
                    i = Object.assign(i, bank);
                    break;
                }
            }
        },
        DELETE_BANK: (state, bank) => {
            for(let i of state.banks){
                if(i.id == bank.id){
                    const index = state.banks.indexOf(i);
                    state.banks.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getBanks({ commit }, payload){
            try{
                let response = await getBanks(payload);
                commit('ADD_BANKS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addBank({ commit }, payload){
            try{
                let response = await addBank(payload);
                let data = response.data;
                commit('ADD_BANK', data.bank);
                return response;
            }catch(e){
                return e;
            }
        },
        async editBank({ commit }, payload){
            try{
                let response = await editBank(payload);
                let data = response.data;
                commit('EDIT_BANK', data.bank);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteBank({ commit }, payload){
            try{
                let response = await deleteBank(payload);
                let data = response.data;
                commit('DELETE_BANK', data.bank);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default bank;