/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getMe, getUsers, addUser, editUser, deleteUser } from '@/services/user';

const user = {
    namespaced: true,
    state: {
        me: {},
        users: [],
        parents: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        me: state => state.me,
        users: state => state.users,
        parents: state => state.parents,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_ME: (state, me) => {
            state.me = me;
        },
        ADD_USERS: (state, payload) => {
            state.users = payload.users;
            Object.assign(state.pagination, payload.pagination);
        },
        ADD_USER: (state, user) => {
            state.users.push(user);
        },
        EDIT_USER: (state, user) => {
            for(let i of state.users){
                if(i.id == user.id){
                    i = Object.assign(i, user);
                    break;
                }
            }
        },
        DELETE_USER: (state, user) => {
            for(let i of state.users){
                if(i.id == user.id){
                    const index = state.users.indexOf(i);
                    state.users.splice(index, 1);
                    break;
                }
            }
        },
        ADD_PARENTS: (state, parents) => {
            state.parents = parents;
        }
    },
    actions: {
        async getMe({ commit }, payload){
            try{
                let response = await getMe(payload);
                commit('ADD_ME', response.data.me);
                return response.data.me;
            }catch(e){
                return e;
            }    
        },
        async getUsers({ commit }, payload){
            try{
                let response = await getUsers(payload);
                commit('ADD_USERS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addUser({ commit }, payload){
            try{
                let response = await addUser(payload);
                let data = response.data;
                commit('ADD_USER', data.user);
                return response;
            }catch(e){
                return e;
            }
        },
        async editUser({ commit }, payload){
            try{
                let response = await editUser(payload);
                let data = response.data;
                commit('EDIT_USER', data.user);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteUser({ commit }, payload){
            try{
                let response = await deleteUser(payload);
                let data = response.data;
                commit('DELETE_USER', data.user);
                return response;
            }catch(e){
                return e;
            }
        },
        async getParents({ commit }, payload){
            try{
                let response = await getUsers(payload);
                let data = response.data;
                commit('ADD_PARENTS', data.users);
                return response.data;
            }catch(e){
                return e;
            }
        },
    }
};

export default user;