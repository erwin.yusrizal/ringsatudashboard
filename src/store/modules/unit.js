/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getUnits, addUnit, editUnit, deleteUnit } from '@/services/unit';

const unit = {
    namespaced: true,
    state: {
        units: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        units: state => state.units,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_UNITS: (state, payload) => {
            state.units = payload.units;
            Object.assign(state.pagination, payload.pagination);
        },
        ADD_UNIT: (state, unit) => {
            state.units.push(unit);
        },
        EDIT_UNIT: (state, unit) => {
            for(let i of state.units){
                if(i.id == unit.id){
                    i = Object.assign(i, unit);
                    break;
                }
            }
        },
        DELETE_UNIT: (state, unit) => {
            for(let i of state.units){
                if(i.id == unit.id){
                    const index = state.units.indexOf(i);
                    state.units.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getUnits({ commit }, payload){
            try{
                let response = await getUnits(payload);
                commit('ADD_UNITS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addUnit({ commit }, payload){
            try{
                let response = await addUnit(payload);
                let data = response.data;
                commit('ADD_UNIT', data.unit);
                return response;
            }catch(e){
                return e;
            }
        },
        async editUnit({ commit }, payload){
            try{
                let response = await editUnit(payload);
                let data = response.data;
                commit('EDIT_UNIT', data.unit);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteUnit({ commit }, payload){
            try{
                let response = await deleteUnit(payload);
                let data = response.data;
                commit('DELETE_UNIT', data.unit);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default unit;