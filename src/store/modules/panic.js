/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getPanics, addPanic, editPanic, deletePanic } from '@/services/panic';

const panic = {
    namespaced: true,
    state: {
        panics: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        panics: state => state.panics,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_PANICS: (state, payload) => {
            state.panics = payload.panics;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_PANIC: (state, panic) => {
            state.panics.push(panic);
        },
        EDIT_PANIC: (state, panic) => {
            for(let i of state.panics){
                if(i.id == panic.id){
                    i = Object.assign(i, panic);
                    break;
                }
            }
        },
        DELETE_PANIC: (state, panic) => {
            for(let i of state.panics){
                if(i.id == panic.id){
                    const index = state.panics.indexOf(i);
                    state.panics.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getPanics({ commit }, payload){
            try{
                let response = await getPanics(payload);
                commit('ADD_PANICS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addPanic({ commit }, payload){
            try{
                let response = await addPanic(payload);
                let data = response.data;
                commit('ADD_PANIC', data.panic);
                return response;
            }catch(e){
                return e;
            }
        },
        async editPanic({ commit }, payload){
            try{
                let response = await editPanic(payload);
                let data = response.data;
                commit('EDIT_PANIC', data.panic);
                return response;
            }catch(e){
                return e;
            }
        },
        async deletePanic({ commit }, payload){
            try{
                let response = await deletePanic(payload);
                let data = response.data;
                commit('DELETE_PANIC', data.panic);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default panic;