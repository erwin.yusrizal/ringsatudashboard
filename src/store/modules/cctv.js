/* eslint-disable no-console */
/* jshint esversion: 6 */

import { getCCTVs, addCCTV, editCCTV, deleteCCTV } from '@/services/cctv';

const cctv = {
    namespaced: true,
    state: {
        cctvs: [],
        pagination: {
            page: 1,
            pages: 1,
            perpage: 25,
            total: 1
        } 
    },
    getters: {
        cctvs: state => state.cctvs,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_CCTVS: (state, payload) => {
            state.cctvs = payload.cctvs;
            Object.assign(state.pagination,payload.pagination);
        },
        ADD_CCTV: (state, cctv) => {
            state.cctvs.push(cctv);
        },
        EDIT_CCTV: (state, cctv) => {
            for(let i of state.cctvs){
                if(i.id == cctv.id){
                    i = Object.assign(i, cctv);
                    break;
                }
            }
        },
        DELETE_CCTV: (state, cctv) => {
            for(let i of state.cctvs){
                if(i.id == cctv.id){
                    const index = state.cctvs.indexOf(i);
                    state.cctvs.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getCCTVs({ commit }, payload){
            try{
                let response = await getCCTVs(payload);
                commit('ADD_CCTVS', response.data);
                return response.data;
            }catch(e){
                console.log(e);
            }    
        },
        async addCCTV({ commit }, payload){
            try{
                let response = await addCCTV(payload);
                let data = response.data;
                commit('ADD_CCTV', data.cctv);
                return response;
            }catch(e){
                return e;
            }
        },
        async editCCTV({ commit }, payload){
            try{
                let response = await editCCTV(payload);
                let data = response.data;
                commit('EDIT_CCTV', data.cctv);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteCCTV({ commit }, payload){
            try{
                let response = await deleteCCTV(payload);
                let data = response.data;
                commit('DELETE_CCTV', data.cctv);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default cctv;