/* jshint esversion: 6 */

import Vue from 'vue';
import Vuex from 'vuex';
import Cookies from 'js-cookie';
import createPersistedState from 'vuex-persistedstate';

import tagsView from './modules/tagsView';
import logs from './modules/logs';
import auth from './modules/auth';
import permission from './modules/permission';
import role from './modules/role';
import bank from './modules/bank';
import paymentMethod from './modules/payment-method';
import unit from './modules/unit';
import region from './modules/region';
import user from './modules/user';
import category from './modules/category';
import media from './modules/media';
import contact from './modules/contact';
import residential from './modules/residential';
import announcement from './modules/announcement';
import residentreport from './modules/residentreport';
import financialreport from './modules/financialreport';
import inventory from './modules/inventory';
import movement from './modules/movement';
import cctv from './modules/cctv';
import panic from './modules/panic';
import panicreport from './modules/panicreport';
import info from './modules/info';
import comment from './modules/comment';
import product from './modules/product';

Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [createPersistedState({
        paths: ['auth.user', 'auth.token']
    })],
    state: {
        sidebar: {
            opened: Cookies.get('sidebar') ? !!+Cookies.get('sidebar') : true,
            withoutAnimation: false
        },
        device: 'desktop'
    },
    getters: {
        sidebar: state => state.sidebar,
        device: state => state.device
    },
    mutations: {
        TOGGLE_SIDEBAR: state => {
            state.sidebar.opened = !state.sidebar.opened;
            state.sidebar.withoutAnimation = false;
            if(state.sidebar.opened){
                Cookies.set('sidebar', 1);
            }else{
                Cookies.set('sidebar', 0);
            }
        },
        CLOSE_SIDEBAR: (state, withoutAnimation) => {
            Cookies.set('sidebar', 0);
            state.sidebar.opened = false;
            state.sidebar.withoutAnimation = withoutAnimation;
        },
        TOGGLE_DEVICE: (state, device) => {
            state.device = device;
        }
    },
    actions: {
        toggleSidebar({ commit }){
            commit('TOGGLE_SIDEBAR');
        },
        closeSidebar({ commit }){
            commit('CLOSE_SIDEBAR');
        },
        toggleDevice({ commit }){
            commit('TOGGLE_DEVICE');
        }
    },
    modules: {
        tagsView,
        logs,
        auth,
        permission,
        role,
        bank,
        paymentMethod,
        unit,
        region,
        user,
        category,
        media,
        contact,
        residential,
        announcement,
        residentreport,
        financialreport,
        inventory,
        movement,
        cctv,
        panic,
        panicreport,
        info,
        comment,
        product
    }
});